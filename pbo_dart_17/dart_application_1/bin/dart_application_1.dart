void main(List<String> arguments) {
  MonsterUcoa m = MonsterUcoa(status: UcoaStatus.confused)
    ..move()
    ..eat();
}

enum UcoaStatus { normal, poisoned, confused }

class MonsterUcoa {
  final UcoaStatus status; // 1: normal , 2 : poisoned ; 3 : confused
  MonsterUcoa({this.status = UcoaStatus.normal});

  void move() {
    switch (status) {
      case UcoaStatus.normal:
        print('Ucoa is moving');
        break;
      case UcoaStatus.poisoned:
        print('Ucoa cannot move. Ucoa is dying. Ucoa needs help.');
        break;
      default:
        print('Ucoa is spinning. Dart language is confusing');
    }
  }

  void eat() {
    print('Ucoa is eating Indomie');
  }
}

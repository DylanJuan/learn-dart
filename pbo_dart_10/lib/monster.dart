import 'package:pbo_dart_10/character.dart';
import 'package:pbo_dart_10/drink_ability_mixin.dart';

abstract class Monster extends character {
  String eatHuman() => "Grr.. Delicious .. Yummy ..";
  String move();
}
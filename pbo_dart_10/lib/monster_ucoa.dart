
import 'package:pbo_dart_10/drink_ability_mixin.dart';

import 'flying_monster.dart';
import 'monster_ubur.dart';

class MonsterUcoa extends MonsterUbur implements FlyingMonster{
  @override
  String fly(){
    return "Terbang terbang melayang";
  }

}


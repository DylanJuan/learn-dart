import 'dart:io';

import 'package:pbo_dart_09/flying_monster.dart';
import 'package:pbo_dart_09/hero.dart';
import 'package:pbo_dart_09/monster.dart';
import 'package:pbo_dart_09/monster_kecoa.dart';
import 'package:pbo_dart_09/monster_ubur.dart';
import 'package:pbo_dart_09/monster_ucoa.dart';

main(List<String> arguments) async{
  Hero h = Hero();

  MonsterUbur u = MonsterUbur();
  List<Monster> monsters=[];

  monsters.add(MonsterUbur());
  monsters.add(MonsterKecoa());
  monsters.add(MonsterUcoa());

  for(Monster m in monsters) {
    if (m is FlyingMonster) {
      print((m as FlyingMonster).fly());
    }
  }

  // h.healthPoint = -10;
  // m.healthPoint = 10;
  //
  // print("Hero HP : "+ h.healthPoint.toString());
  // print("Monster HP : "+ m.healthPoint.toString());
  // print(h.killMonster());
  // print(m.eatHuman());
  // print(u.swin());


}

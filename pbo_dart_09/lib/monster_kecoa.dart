
import 'package:pbo_dart_09/flying_monster.dart';
import 'package:pbo_dart_09/monster.dart';

class MonsterKecoa extends Monster implements FlyingMonster{
  @override
  String fly()=>"Syuuuung ..";

  @override
  String move() {
    return "Jalan-jalan santai";
  }

}
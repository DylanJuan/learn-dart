import 'dart:io';

import 'package:pbo_dart_08/hero.dart';
import 'package:pbo_dart_08/monster.dart';
import 'package:pbo_dart_08/monster_kecoa.dart';
import 'package:pbo_dart_08/monster_ubur.dart';

main(List<String> arguments) async{
  Hero h = Hero();
  Monster m = Monster();
  MonsterUbur u = MonsterUbur();
  List<Monster> monsters=[];

  monsters.add(MonsterUbur());
  monsters.add(MonsterKecoa());

  print((monsters as MonsterUbur).swin());

  for(Monster m in monsters){
    if(m == MonsterUbur()) {
      print(m.eatHuman());
    }
  }

  h.healthPoint = -10;
  m.healthPoint = 10;

  print("Hero HP : "+ h.healthPoint.toString());
  print("Monster HP : "+ m.healthPoint.toString());
  print(h.killMonster());
  print(m.eatHuman());
  print(u.swin());


}


class PersegiPanjang {
  double _panjang;
  double _lebar;

  void set lebar(double value){ //properti
    if(value< 0){
      value *= -1;
    }
    _lebar = value;
  }

  void setPanjang(double value){ //setter getter
    if(value<0){
      value *= -1;
    }
    _panjang = value;
  }

  double getPanjang(){ //getter
    return _panjang;
  }

  double get lebar{ //properti
    return _lebar;
}

  double hitungLuas() {
    return this._panjang * this._lebar;
  }

  double get luas{
    return _panjang*_lebar;
  }

}
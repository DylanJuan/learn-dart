void main(List<String> arguments) {
  // String myString;
  // dynamic myDynamic;

  // myDynamic = 'Hello';
  // myDynamic = 12;
  // myDynamic = Person();

  // // print((myDynamic as Person).name);
  // if (myDynamic is Person) {
  //   print(myDynamic.name);
  // }
  // var myVar;
  // myVar = 12;
  // myVar = 'hello';
  // print(myVar);
  var myVar = Person();
  print(myVar.name);
}

class Person {
  String name = 'no name';
}

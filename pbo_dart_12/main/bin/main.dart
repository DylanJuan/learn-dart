import 'package:main/main.dart';

void dart(List<String> arguments) {
  var p = Person('David', doingHobby: (_) {
    print('Swimming in the pool');
  });

  p.takeARest();
}

// void davidsHobby(String name) {
//   print('$name is swimming');
// }

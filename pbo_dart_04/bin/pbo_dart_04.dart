import 'dart:io';

String say(String from, String message, {String to, String appName}) {
  return from +
      " say " +
      message +
      ((to != null) ? " to " + to : "") +
      ((appName != null) ? " via " + appName : "");
}

String say2(String from, String message, [String to, String appName = "Whatsapp"]) {
  return from +
      " say " +
      message +
      ((to != null) ? " to " + to : "") +
      ((appName != null) ? " via " + appName : "");
}

double luas_double(double panjang, double lebar) {
  double hasil;
  hasil = panjang * lebar;
  return hasil;
}

void sapa_penonton() {
  print("hello penonton");
}

double luas_segiempat(double panjang, double lebar)=> panjang*lebar;

int doMathOperator(int number1, int number2, Function (int,int)operator){
  return operator(number1,number2);
}


main(List<String> arguments) {
  Function f;
  f =luas_segiempat;
  print(say("Johnny", "Hello", to: "Doloris"));
  print(say2("Johnny", "Hello", "Doloris"));
  sapa_penonton();
  print(f(6.0,3.0));
  print(doMathOperator(1, 2, (a,b)=>a*b));

  // double p,l,luas;
  // p = double.tryParse(stdin.readLineSync());
  // l = double.tryParse(stdin.readLineSync());
  // luas = luas_double(p,l);
  // print(luas);
}
